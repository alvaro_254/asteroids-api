package com.api.asteroid.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/asteroids")
public class asteroidController {

	@GetMapping(value = "/helloWorld")
	public String helloWorld() {
		return "Hello World!";
	}
}
